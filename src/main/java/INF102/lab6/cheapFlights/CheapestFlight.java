package INF102.lab6.cheapFlights;

import java.util.LinkedList;
import java.util.List;
import INF102.lab6.graph.WeightedDirectedGraph;

public class CheapestFlight implements ICheapestFlight {

    @Override
    public WeightedDirectedGraph<City, Integer> constructGraph(List<Flight> flights) {
        WeightedDirectedGraph<City, Integer> graph = new WeightedDirectedGraph<>();
        for (Flight flight : flights) {
            City from = flight.start;
            City to = flight.destination;
            graph.addVertex(from);
            graph.addVertex(to);
            graph.addEdge(from, to, flight.cost);
        }
        return graph;
    }

    @Override

    public int findCheapestFlights(List<Flight> flights, City start, City destination, int nMaxStops) {
        WeightedDirectedGraph<City, Integer> graph = constructGraph(flights);
        int[] lowestCost = new int[] { Integer.MAX_VALUE }; // Using array to allow
        // modification within the method
        findPaths(graph, start, destination, nMaxStops + 1, new LinkedList<>(), 0,
                lowestCost); // nMaxStops + 1 to include the start city
        return lowestCost[0];
    }

    private void findPaths(WeightedDirectedGraph<City, Integer> graph, City currentCity, City destination, int stopsLeft,
            LinkedList<City> currentPath, int currentCost, int[] lowestCost) {
        currentPath.add(currentCity);

        // highsocre system
        if (currentCity.equals(destination)) {
            if (lowestCost[0] > currentCost) {
                lowestCost[0] = currentCost;
            }
        } else if (stopsLeft > 0) {

            for (City neighbor : graph.outNeighbours(currentCity)) {
                if (!currentPath.contains(neighbor)) { // To avoid cycles
                    Integer edgeWeight = graph.getWeight(currentCity, neighbor);
                    findPaths(graph, neighbor, destination, stopsLeft - 1, currentPath, currentCost +
                            edgeWeight, lowestCost);
                }
            }
        }

        currentPath.removeLast(); // Backtrack
    }
}
